class Report:
	def __init__(self, oldindex, newindex):
		self.oldindex = oldindex
		self.newindex = newindex

		self.oldapps = self.oldindex.idx['apps']
		self.newapps = self.newindex.idx['apps']

	@staticmethod
	def _format_app_for_twif(index, pkgid, archive=False):
		pkg = index.idx['apps'][pkgid]
		if archive:	return "**{}: {}**".format(pkg["localized"]["en-US"]['name'], pkgid)
		else:		return "**[{}](https://f-droid.org/app/{})**".format(pkg["localized"]["en-US"]['name'], pkgid)

	def _get_formatted_changelog_link(self, pkgid, text="updated"):
		try:
			return "[{}]({})".format(text, self.newapps[pkgid]['changelog'].strip())
		except KeyError:
			return text

	def is_added_app(self, appid):
		return appid not in self.oldapps

	def get_added_apps(self):
		r = []
		for pkgid in sorted(self.newapps.keys()):
			if self.is_added_app(pkgid):
				try:
					description = ": " + self.newapps[pkgid]['summary'].strip().capitalize()
					if not description.endswith("."): description = description + "."
				except KeyError:
					description = ""
				s = "* {}{}\n".format(self._format_app_for_twif(self.newindex, pkgid), description)
				r.append(s)
		return r

	def is_removed_app(self, appid):
		return appid not in self.newapps

	def get_removed_apps(self):
		r = []
		for pkgid in sorted(self.oldapps.keys()):
			if self.is_removed_app(pkgid):
				s = "* {}\n".format(self._format_app_for_twif(self.oldindex, pkgid, archive=True))
				r.append(s)
		return r

	def _get_updated_apps(self):
		downgraded, updated, beta_updated = [], [], []

		for pkgid in sorted(self.oldapps.keys()):
			if pkgid not in self.newapps: continue

			oldav = self.oldindex.get_available_version_codes(pkgid)
			oldvc = int(self.oldapps[pkgid]['suggestedVersionCode'])
			if oldvc not in oldav: oldvc = oldav[-1]

			newav = self.newindex.get_available_version_codes(pkgid)
			newvc = int(self.newapps[pkgid]['suggestedVersionCode'])
			if newvc not in newav: newvc = newav[-1]

			if newvc < oldvc:
				appstr = self._format_app_for_twif(self.newindex, pkgid)
				s = "* Downgraded?!: {}: {!r} -> {!r}\n".format(appstr, oldvc, newvc)
				downgraded.append(s)

			if newvc > oldvc:
				oldver = self.oldindex.get_version_for_versioncode(pkgid, oldvc)
				newver = self.newindex.get_version_for_versioncode(pkgid, newvc)
				appstr = self._format_app_for_twif(self.newindex, pkgid)
				updstr = self._get_formatted_changelog_link(pkgid)
				s = "* {} was {} from {} to {}\n".format(appstr, updstr, oldver, newver)
				updated.append(s)

			if newav[-1] not in oldav and newav[-1] != newvc:
				oldver = self.oldindex.get_version_for_versioncode(pkgid, oldav[-1])
				newver = self.newindex.get_version_for_versioncode(pkgid, newav[-1])
				appstr = self._format_app_for_twif(self.newindex, pkgid)
				updstr = self._get_formatted_changelog_link(pkgid)
				s = "* {} was {} from {} to {}\n".format(appstr, updstr, oldver, newver)
				beta_updated.append(s)

		return downgraded, updated, beta_updated

	def get_downgraded_apps(self):
		return self._get_updated_apps()[0]

	def get_updated_apps(self):
		return self._get_updated_apps()[1]

	def get_beta_updated_apps(self):
		return self._get_updated_apps()[2]

