import git
import logging
import json
import os
import shutil
import sys
import subprocess
import shlex
import fnmatch
from datetime import datetime
from datetime import date
from pathlib import Path
from dictionary_diff import dict_diff

class App:
    """
    Class to represent App/packagename body
    """
    def __init__(self, package_name, current_index, past_index):
        self.package_name = package_name
        self.name = self._get_app_name(current_index, package_name, past_index)
        self.current_checksum = self._get_current_checksum(current_index, package_name)
        self.past_checksum = self._get_previous_checksum(past_index, package_name)
        self.previous_version_code = self._get_past_version_code(past_index, package_name)
        self.current_version_code = self._get_current_version_code(current_index, package_name)
        self.previous_version_name = self._get_past_version_name(past_index, package_name)
        self.current_version_name = self._get_current_version_name(current_index, package_name)
        self.app_status = self._get_app_status()

    def _get_app_name(self, current_index, package_name, past_index):
        try:
            app_name = current_index['packages'][package_name]["metadata"]["name"]["en-US"]
        except KeyError:
            app_name = past_index['packages'][package_name]["metadata"]["name"]["en-US"]
        finally:
            return app_name
        
    def _get_current_checksum(self, current_index, package_name):
        try:
            current_index['packages'][package_name]
        except KeyError:
            return None
        else:
            return list(current_index['packages'][package_name]['versions'].keys())[0]

    def _get_previous_checksum(self, past_index, package_name):
        try:
            past_index['packages'][package_name]
        except KeyError:
            return None
        else:
            return list(past_index['packages'][package_name]['versions'].keys())[0]

    def _get_past_version_code(self, past_index, package_name):
        if self.past_checksum is None:
            return None
        return past_index['packages'][package_name]['versions'][self.past_checksum]['manifest']['versionCode']

    def _get_current_version_code(self, current_index, package_name):
        if self.current_checksum is None:
            return None
        return current_index['packages'][package_name]['versions'][self.current_checksum]['manifest']['versionCode']
        

    def _get_current_version_name(self, current_index, package_name):
        if self.current_checksum is None:
            return None
        return current_index['packages'][package_name]['versions'][self.current_checksum]['manifest']['versionName']

    def _get_past_version_name(self, past_index, package_name):
        if self.past_checksum is None:
            return None
        return past_index['packages'][package_name]['versions'][self.past_checksum]['manifest']['versionName']

    def _get_app_status(self):
        if self.current_version_code is not None and self.previous_version_code is not None:
            if self.current_version_code > self.previous_version_code:
                return "updated"
            if self.previous_version_code > self.current_version_code:
                return "downgraded"
            if self.current_version_code == self.previous_version_code:
                return "no change"
        if self.current_version_code is None and self.previous_version_code is not None:
            return "removed"
        if self.previous_version_code is None and self.current_version_code is not None:
            return "new"


def generate_diffs_of_new_and_previous_indexes(old_index, new_index):
    current_index_v2 = json.load(open(new_index))
    #old_index = "/home/vagrant/twif-utli/twif-util/sample-indexes/sample-index-v2-json/index-v2-2022-10-31.json"
    previous_index_v2 = json.load(open(old_index))

    previous_current_index_diff = dict_diff(previous_index_v2, current_index_v2)
    if previous_current_index_diff:
        packages_list = list(previous_current_index_diff['packages'].keys())
        removed_apps_list = list()
        added_apps_list = list()

        #Get upgraded and downgraded apps
        list_of_downgraded_apps = list()
        list_of_upgraded_apps = list()
        
        for app in packages_list:
            app_body = App(app, current_index_v2, previous_index_v2)
            if app_body.app_status == "updated":
                list_of_upgraded_apps.append(app_body)
            if app_body.app_status == "downgraded":
                list_of_downgraded_apps.append(app_body)
            if app_body.app_status == "removed":
                removed_apps_list.append(app_body)
            if app_body.app_status == "new":
                added_apps_list.append(app_body)
        return added_apps_list, removed_apps_list, list_of_downgraded_apps , list_of_upgraded_apps
    else:
        print("No diffs found current index matches previous index")
        todays_date = str(datetime.date(datetime.now()))
        file_name = "index-v2-" + todays_date + ".json"
        os.remove(file_name)
        sys.exit()


def display_output(diff_tuple):
    """displays current output to file"""
    added_apps = diff_tuple[0]
    removed_apps = diff_tuple[1]
    downgraded_apps = diff_tuple[2]
    upgraded_apps = diff_tuple[3]
    todays_date = date.today()
    day_and_date = todays_date.strftime("%A, %d %b %Y")
    week_number = todays_date.isocalendar().week
    f_droid_wiki_str = "https://f-droid.org/wiki/page/"

    print("### Index difference report")
    print("\n")
    print(f"Report generated on the {day_and_date} ,Week {week_number}")
    print("\n")

    if removed_apps:
        num_removed_apps = str(len(removed_apps))
        print("#### Removed Apps")
        for app in removed_apps:
            print(app.name)
        print("\n")
        print(f"{num_removed_apps} were removed")
    
    
    if added_apps:
        num_added_apps = str(len(added_apps))
        print("#### Added Apps")
        for app in added_apps:
            app_str = f"* **[{app.name}]({f_droid_wiki_str}{app.package_name})**"
            print(app_str)
        print("\n")
        print(f"{num_added_apps} were added")
     
    
    if downgraded_apps:
        print("\n")
        num_downgraded_apps = str(len(downgraded_apps))
        print("#### Downgraded Apps")
        for app in downgraded_apps:
            app_str = f"* **[{app.name}]({f_droid_wiki_str}{app.package_name})** was downgraded from {app.previous_version_name} to {app.current_version_name}"
            print(app_str)
        print("\n")
        print(f"{num_downgraded_apps} were downgraded")

    if upgraded_apps:
        print("\n")
        num_upgraded_apps = str(len(upgraded_apps))
        print("#### Upgraded Apps")
        for app in upgraded_apps:
            app_str = f"* **[{app.name}]({f_droid_wiki_str}{app.package_name})** was updated from {app.previous_version_name} to {app.current_version_name}"
            print(app_str)
        print("\n")
        print(f"{num_upgraded_apps} were upgraded")
    


def get_current_index_v2_json():
    """Gets current index-v2 from f-droid.org"""
    index_v2_url = "https://f-droid.org/repo/index-v2.json"
    todays_date = str(datetime.date(datetime.now()))
    complete_command = "wget  --tries=5 --quiet -O index-v2-" + todays_date + ".json " + index_v2_url
    print("Fetching index-v2.json from f-droid.org")
    fetch_index = subprocess.run(shlex.split(complete_command))
    

    if fetch_index.returncode != 0:
        file_name = "index-v2-" + todays_date + ".json"
        os.remove(file_name)
        logging.warning("wget cannot fetch index-v2.json")
        sys.exit()


def set_up_git_repo_for_index_v2_json(
    index_v2_repo='index_v2_git_repo'
):
    """This function sets up git repo with git lfs"""
    if os.path.exists(os.path.join(index_v2_repo, '.git')):
        gitrepo = git.Repo(index_v2_repo)
    else:
        if not os.path.exists(index_v2_repo):
            os.mkdir(index_v2_repo)
        gitrepo = git.Repo.init(index_v2_repo)

def get_any_index_v2_json(any_date, file_path):
    """Uses fnmatch to get index-v2.json file given the date"""
    match_str = "index-v2-" + any_date + ".json"
    for json_file in os.listdir(file_path):
        if fnmatch.fnmatch(json_file, match_str):
            return json_file

def check_git_repo_for_any_commits():
    """Checks index v2 git repo for any commits"""
    index_v2_git_location = str(Path.cwd()) + "/" + 'index_v2_git_repo'
    repo = git.Repo(index_v2_git_location)
    commit_list = repo.head.reference.log()
    todays_date = str(datetime.date(datetime.now()))

    if commit_list:
        #look for the last commit and retrieve the file
        last_commit = commit_list[-1]
        time_of_last_commit = last_commit.time[0]
        date_of_last_commit = str(datetime.date(datetime.fromtimestamp(time_of_last_commit)))
        #use date_of_last_commit to look for file in the git repo
        last_index_json = get_any_index_v2_json(date_of_last_commit, index_v2_git_location)
        complete_path_of_previous_index = index_v2_git_location + "/" + last_index_json
        #this is where we read both files and generate the diff
        latest_index_json = get_any_index_v2_json(todays_date, str(Path.cwd()))
        diff_tuple = generate_diffs_of_new_and_previous_indexes(complete_path_of_previous_index, latest_index_json)
        display_output(diff_tuple)
        source = str(Path.cwd()) + "/" + latest_index_json
        dest = index_v2_git_location + "/" + latest_index_json
        shutil.move(source,dest)
        repo.index.add([latest_index_json])
        commit_meesage = f"F-Droid Index-V2-json as per {todays_date}"
        repo.index.commit(commit_meesage)

    else:
        #add the index-v2-date in the repo and commit
        #stop execution and tell user of stopped execution
        logging.info("No commits found in index v2 repo")
        latest_index_json = get_any_index_v2_json(todays_date, str(Path.cwd()))
        #move file to git repo and commit it
        shutil.move(str(Path.cwd()) + "/" + latest_index_json, index_v2_git_location + "/" + latest_index_json)
        repo.index.add([latest_index_json])
        commit_meesage = f"F-Droid Index-V2-json as per {todays_date}"
        repo.index.commit(commit_meesage)
        print("No diff generated, this is the first time you are running TWIF with Index-v2.json")

def main():
    set_up_git_repo_for_index_v2_json()
    get_current_index_v2_json()
    check_git_repo_for_any_commits()

if __name__ == "__main__":
    main()
