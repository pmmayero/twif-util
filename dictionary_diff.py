
def dict_diff(source, target):
    """
    This is the function used to show diffs in index v2
    it can be found at https://gitlab.com/fdroid/fdroidserver/-/blob/master/fdroidserver/index.py#L477
    """
    if not isinstance(target, dict) or not isinstance(source, dict):
        return target

    result = {key: None for key in source if key not in target}

    for key, value in target.items():
        if key not in source:
            result[key] = value
        elif value != source[key]:
            result[key] = dict_diff(source[key], value)

    return result