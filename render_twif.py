# -*- coding: utf-8 -*-

import collections
import os
import pprint
import re
import warnings

import Index
import debug
import nocurses

App = collections.namedtuple('App', ('name', 'id'))

def find_app_by_name(sequencematcher, names, name):
	sequencematcher.set_seq1(name.lower())
	candidates = []
	for n, pkgid in names.items():
		sequencematcher.set_seq2(n.lower())

		r = sequencematcher.ratio()
		if len(candidates) < 3:
			candidates.append((r, n, pkgid))
		elif r > candidates[-1][0]:
			candidates[-1] = (r, n, pkgid)
			candidates.sort(reverse=True)

	debug.dprint("{!r}: {!r}".format(name, candidates))

	return App(candidates[0][1], candidates[0][2])

RE_MDLINK = re.compile("^\\[([^]]+)\\]\\(([^)]+)\\)$")

def format_replacement(keyword, replacement, link_context):
	m = RE_MDLINK.search(replacement)

	if link_context and m:
		return m.group(2)
	elif link_context:
		return replacement
	elif replacement.startswith("http://") or replacement.startswith("https://"):
		return "[{}]({})".format(keyword, replacement)

	return replacement

def escape_md(text):
	result = []
	for char in text:
		if char in "*_[]": result.append("\\")
		result.append(char)
	return "".join(result)

def main(config):
	import difflib

	names = {}
	packages = {}

	RE_APP = re.compile("(\\]\\()?\\{([!@?]?)([^{}<% \n][^{}\n]*)\\}(\\))?")
	RE_VIM = re.compile("\n[Vv][Ii][Mm]?:[^\n]*\n")

	index = Index.get_latest_index(config)

	for appid, app in index.idx['apps'].items():

		try:
			name = app['name'].strip()
			pkgid = app['packageName'].strip()
			names[name] = pkgid.strip()
			packages[pkgid.strip()] = name
		except KeyError:
			debug.dprint("! --noname--", repr(appid))
			continue

		for language in config['languages']:
			try:
				localname = app['localized'][language]['name'].strip()
				names[localname] = pkgid.strip()
				packages[pkgid.strip()] = localname
			except KeyError:
				pass

	article_fn = config['article_source']
	with open(article_fn, "r") as f:
		article = f.read()

	article = RE_VIM.sub("\n", article, 1)

	result = []
	prev_pos = 0

	nocurses.setdefault('red')

	s = difflib.SequenceMatcher()
	for m in RE_APP.finditer(article):
		sigil = m.group(2)
		word = m.group(3)
		link_context = m.group(1) and m.group(4)

		if sigil == "":
			replacement = format_replacement(word, config['overrides'][word.lower()], link_context)
			debug.dprint("Override:", word, replacement)
		elif sigil == "?":
			app = find_app_by_name(s, names, word)
			name = escape_md(app.name)
			if link_context:
				replacement = "https://f-droid.org/app/{}".format(app.id)
			else:
				replacement = "**[{}](https://f-droid.org/app/{})**".format(name, app.id)
		elif sigil == "!":
			app = App(packages[word], word)
			name = escape_md(app.name)
			if link_context:
				replacement = "https://f-droid.org/app/{}".format(name, app.id)
			else:
				replacement = "**[{}](https://f-droid.org/app/{})**".format(name, app.id)
			debug.dprint("PackageID:", word, replacement)
		elif sigil == "@":
			if link_context:
				replacement = format_replacement(word, config['users'][word.lower()], link_context)
			else:
				replacement = "**" + format_replacement(word, config['users'][word.lower()], link_context) + "**"
			debug.dprint("User:", word, replacement)
		else:
			replacement = "!!!???!!!"
			debug.dprint("!!Error!!:", word, replacement)

		pos = m.start()
		result.append(article[prev_pos:pos])
		result.append(m.group(1) or "")
		result.append(replacement)
		result.append(m.group(4) or "")
		prev_pos = m.end()

	result.append(article[prev_pos:])

	rendered_article_fn = config['article_destination']
	with open(rendered_article_fn, "w") as f:
		print("".join(result), file=f, end="")

	debug.cdprint("\"{}\" written".format(rendered_article_fn), fg='green')
	nocurses.cleardefault()

if __name__ == '__main__':
	from Config import Config
	config = Config().load("untracked/config.json")
	main(config)
