#!/bin/bash

### Compress a series of related files (evolving versions of the same data)
### by comparing consecutive files and replacing all but the first file with
### a diff. The diffs are then further compressed.
###
### For additional bonus points, the md5sum of the original is added to the
### name of the diff, so the reconstituted file can be verified.

eecho ()
{
	echo "$@" 1>&2
}

failed ()
{
	eecho "Failed" "$@"
	exit 1
}

try ()
{
	eecho "> $*"
	"$@" || failed to execute \""$@"\"
}

AWK_GET_MD5='
	BEGIN {
		for (x=0; x < 32; x++) p = p "[0-9a-f]"
		p = "\\." p "\\."
	}
	{
		if (match($0, p)) print substr($0, RSTART+1, RLENGTH-2)
	}
'

FILES=( "$@" )

# Scan all files and generate diffs
for (( i="$#"; i > 1; i-- )); do
	P="${FILES[$i - 2]}"	# Previous
	F="${FILES[$i - 1]}"	# Current
	O="$F".old		# Current - moved to old
	MD5="$( md5sum "$F" | awk '{print $1}' )"
	D="$F"."$MD5".diff	# Diff
	DZ="$D".xz		# Compressed diff

	echo "$D" 1>&2
	echo "diff -e $P $F > $D" 1>&2

	if cmp -s "$P" "$F"; then
		# Files are identical - create a zero-length uncompressed "diff" file
		echo -n > "$D"
	else
		# Files differ - create compressed diff
		export P F
		(	echo "e $P"
			diff -e "$P" "$F"
			echo "w $F"
		) | xz > "$DZ"
	fi

	# Verification: extract and compare md5sum from fileNAME
	MD52="$( echo "$D" | awk "$AWK_GET_MD5" )"
	[ "$MD5" = "$MD52" ] || failed to extract md5sum from filename \""$D"\"

	# Move the old file out of the way - test the result with md5
	try mv -vn "$F" "$O"
	[ -e "$F" ] && failed to move \""$F"\": file still exists
	MD53="$( md5sum "$O" | awk '{print $1}' )"
	[ "$MD5" = "$MD53" ] || failed to verify md5sum for \""$O"\": expected "$MD5", got "$MD53"

	# Verify the patch by applying it, regenerating the original file
	if [ -f "$D" -a ! -s "$D" ] && cmp "$P" "$O"; then
		# Files are already identical - no patching needed
		eecho "Files \"$P\" and \"$O\" are identical, and the diff exists and is zero"
	elif [ -f "$DZ" -a -s "$DZ" ]; then
		# Apply patch, verify, delete
		< "$DZ" try xz -dc | try ed -r
		MD54="$( md5sum "$F" | awk '{print $1}' )"
		[ "$MD5" = "$MD54" ] || failed to verify md5sum for recreated \""$F"\": expected "$MD5", got "$MD54"
		cmp "$O" "$F" || failed to compare \""$O"\" and \""$F"\": files differ
		rm "$F"
	else
		failed to verify the diff \""$DZ"\": an unknown condition occurred
	fi
done
